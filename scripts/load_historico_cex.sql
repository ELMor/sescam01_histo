-- SESCAM: Carga Historico
-- Script: load_historico_consultas.sql
-- Creaci�n del esquema (tablas) del hist�rico.
-- 1. Crear procedimiento legate.InitHistoricoConsultas
CREATE OR REPLACE PROCEDURE InitHistoricoConsultas (dias NUMBER) IS


CURSOR histo_lespcex (dias NUMBER) IS
SELECT cega,ncita
FROM LESPCEX
WHERE (catalog = 'HISTOR' OR CATALOG IS NULL) AND fechacit < TRUNC(SYSDATE) - dias;

COUNTER NUMBER :=0;

rc histo_lespcex%ROWTYPE;

BEGIN

    FOR rc IN histo_lespcex (dias) LOOP
    	-- inserciones
		DELETE FROM CEXAPL_HISTO WHERE centro = rc.cega AND ncita = rc.ncita;
		INSERT INTO CEXAPL_HISTO SELECT * FROM cexapl WHERE centro = rc.cega AND ncita = rc.ncita;

		-- borrados
		DELETE FROM CEX_APLDEM WHERE centro = rc.cega AND ncita = rc.ncita;
		DELETE FROM REPROGRAMA WHERE centro = rc.cega AND ncita = rc.ncita;
		DELETE FROM CEXINCID WHERE centro = rc.cega AND ncita = rc.ncita;
		DELETE FROM SOLAPES WHERE centro = rc.cega AND claveinicial = rc.ncita;
		DELETE FROM CONTINUOS WHERE centro = rc.cega AND claveinicial = rc.ncita;
		-- insercion en lespcex
    	DELETE FROM LESPCEX_HISTO WHERE cega = rc.cega AND ncita = rc.ncita;
 	    INSERT INTO LESPCEX_HISTO
			(cip,cega,ncita,hicl,cias,ape1,ape2,nomb,fnac,sexo,domi,pobl,cpos,prov,tel1,tel2
			,slab,fina,gara,area,area_des,servproc,servproc_des,medipeti,medipeti_des
			,servreal,servreal_des,agen,agen_des,sala,sala_des,cmedreal,medireal,finclusi
			,prestaci,prestaci_des,priorida,fechacit,fechader,centrder,especder
			,fsoldem1,fsoldem2,fsoldem3,fsoldem4,fsoldem5,tdemsol1,tdemsol2,tdemsol3,tdemsol4,tdemsol5
			,fsalida,motisali,indinoga,observac,	finclusihos,	identpres,	tvisita,catalog )
		 SELECT cip,cega,ncita,hicl,cias,ape1,ape2,nomb,fnac,sexo,domi,pobl,cpos,prov,tel1,tel2
			,slab,fina,gara,area,area_des,servproc,servproc_des,medipeti,medipeti_des
			,servreal,servreal_des,agen,agen_des,sala,sala_des,cmedreal,medireal,finclusi
			,prestaci,prestaci_des,priorida,fechacit,fechader,centrder,especder
			,fsoldem1,fsoldem2,fsoldem3,fsoldem4,fsoldem5,tdemsol1,tdemsol2,tdemsol3,tdemsol4,tdemsol5
			,fsalida,motisali,indinoga,observac,	finclusihos,	identpres,	tvisita,catalog
    	FROM LESPCEX WHERE cega = rc.cega AND ncita = rc.ncita;
		DELETE FROM LESPCEX WHERE cega = rc.cega AND ncita = rc.ncita;
		COUNTER := COUNTER + 1;
		IF COUNTER MOD 1000 = 0 THEN
			COMMIT;
		END IF;
	END LOOP;
	COMMIT;

  -- purgado de masterclaves.
  Delete From masterclave m Where m.tipo = 2 And Not Exists (Select centro From lespcex l Where l.cega = m.centro And l.ncita = m.clave);
  
  	
  commit;

END InitHistoricoConsultas;
/
-- 2. Ejecutar procedimiento
BEGIN legate.InithistoricoConsultas(150); END; 
