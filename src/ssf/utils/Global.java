package ssf.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.io.InputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Properties;
import org.apache.log4j.PropertyConfigurator;


/**
 * <p>Title: Recopilacion Listas de Espera SESCAM</p>
 * <p>Description: Sistema de recopilacion de Informix HP-HIS de listas de espera</p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */

public class Global {

  /**
   * Devuelve un String con la fecha y hora actual
   * @return Fecha y hora actual formateada para realizar logging
   */
  public static String now() {
    SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    String now = df.format(new Date(System.currentTimeMillis())).toString();
    return "[" + now + "]";
  }

  /**
   * Comprueba si rs(field), que es un campo tipo fecha es anterior a ahora
   * @param rs ResultSet que contiene el campo fecha
   * @param field Nombre del campo fecha dentro de rs
   * @return true si rs(field) es anterior a ahora
   */
  public static boolean isBeforeNow(ResultSet rs, String field) {

    try {
      Timestamp ts = rs.getTimestamp(field);
      if (ts == null) {
        return true;
      }
      if (ts.getTime() < System.currentTimeMillis()) {
        return true;
      }
    }
    catch (Exception e) {
    }
    return false;
  }

  /**
   * Cambia una subcadena por otra
   *
   * @param main Cadena principal donde se va a sustituir
   *
   * @param search Cadena a reemplazar
   *
   * @param replace Cadena que reemplaza
   *
   * @return La cadena de caracteres main con la/s subcadenas 'sarch' cambiadas
   *         por 'replace'
   *
   */
  public static String replaceString(String main, String search, String replace) {
    for (int pos = main.indexOf(search);
         pos >= 0;
         pos = main.indexOf(search, pos + replace.length())) {
      main = main.substring(0, pos) + replace +
          main.substring(pos + search.length());
    }
    return main;

  }

  /**
   * Retorna como un String el contenido del archivo 'file'
   *
   * @param file Nombre del archivo
   *
   * @exception IOException Si hubo algun problema abriendo el fichero
   *
   */
  public static String getContentOfFile(String file) throws IOException {
    InputStream fis = ClassLoader.getSystemClassLoader().
        getSystemResourceAsStream(file);
    StringBuffer sb = new StringBuffer();
    int c;
    while ( (c = fis.read()) != -1) {
      sb.append( (char) c);
    }
    String cmd = sb.toString();
    sb = null;
    return cmd;
  }

  /**
   * Retorna el contenido de un fichero como conjunto de cadenas delimitadas
   *
   * @param file Nombre del archivo
   *
   * @param delim Delimitador. Deben coincidir completamente
   *
   * @param lines Array donde se devuelven las lineas encontradas.
   *
   * @return Numero de cadenas encontradas en el archivo
   *
   */
  public static int getContentOfFileAsScript(String file, String delim,
                                             String lines[]) throws Exception {
    String cof = getContentOfFile(file);
    int currentline = 0;
    for (int begpos = 0, endpos = cof.indexOf(delim);
         endpos > 0;
         begpos = endpos+delim.length(), endpos = cof.indexOf(delim, begpos)) {
      if (currentline >= lines.length) {
        throw new Exception("Maximo numero de lineas de script excedido");
      }
      String lineaCRLF= cof.substring(begpos, endpos);
      String lineaLF=Dos2Unix(lineaCRLF);
      lines[currentline]=lineaLF;
      currentline++;
    }
    return currentline;
  }

  /**
   * Cambia el retorno de carro DOS por el de Unix
   *
   * @param txt Texto donde se cambia
   *
   * @return Texto cambiado
   *
   */
  public static String Dos2Unix(String txt){
    return replaceString(txt,"\r\n","\n");
  }

  /**
   * Cambia las cadenas que se encuentren en par por su valor
   *
   * @param main Texto en el que efectuar los reemplazos
   *
   * @param par Contiene pares (key-string,value-string). Se busca key-string y
   *            se sustituye por value-string
   *
   * @return Texto cambiado
   *
   */
  public static String multipleReplace(String main, Hashtable par) {
    for (Enumeration e = par.keys(); e.hasMoreElements(); ) {
      String search = (String) e.nextElement();
      String replac = (String) par.get(search);
      main = replaceString(main, search, replac);
    }
    return main;
  }

  /**
   * Inicializa el sistema de loggin mediante un fichero properties.
   *
   * @param propFile Definicion LOG4J de Apache Group
   *
   */
  public static void initLog4J(String propFile) throws Exception {
    //Inicializamos el sistema de log
    String log4jFileName = propFile;
    Properties log4jProp = new Properties();
    InputStream log4jIS = ClassLoader.getSystemResourceAsStream(log4jFileName);
    log4jProp.load(log4jIS);
    PropertyConfigurator.configure(log4jProp);
  }
}