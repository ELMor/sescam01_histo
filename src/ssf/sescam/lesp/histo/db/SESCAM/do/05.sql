CREATE OR REPLACE PROCEDURE legate.Inithistorico (dias NUMBER) IS

 
CURSOR histo_lespcex (dias NUMBER) IS
SELECT cega,ncita
FROM LESPCEX
WHERE catalog = 'HISTOR' AND fechacit < TRUNC(SYSDATE) - dias;

CURSOR histo_lesp (dias NUMBER) IS
SELECT centro,norden
FROM LESP
WHERE catalog = 'HISTOR' AND fsal < TRUNC(SYSDATE) - dias;

COUNTER NUMBER :=0;
rc histo_lespcex%ROWTYPE;
rc1 histo_lesp%ROWTYPE;

BEGIN

    FOR rc IN histo_lespcex (dias) LOOP
    	-- inserciones
		DELETE FROM CEXAPL_HISTO WHERE centro = rc.cega AND ncita = rc.ncita;   	
		INSERT INTO CEXAPL_HISTO SELECT * FROM cexapl WHERE centro = rc.cega AND ncita = rc.ncita;
		
		-- borrados
		DELETE FROM CEX_APLDEM WHERE centro = rc.cega AND ncita = rc.ncita;   	
		DELETE FROM REPROGRAMA WHERE centro = rc.cega AND ncita = rc.ncita;   	
		DELETE FROM CEXINCID WHERE centro = rc.cega AND ncita = rc.ncita;   	
		DELETE FROM SOLAPES WHERE centro = rc.cega AND claveinicial = rc.ncita;   	
		DELETE FROM CONTINUOS WHERE centro = rc.cega AND claveinicial = rc.ncita;   	
		-- insercion en lespcex
    	DELETE FROM LESPCEX_HISTO WHERE cega = rc.cega AND ncita = rc.ncita;
 	    INSERT INTO LESPCEX_HISTO 
			(cip,cega,ncita,hicl,cias,ape1,ape2,nomb,fnac,sexo,domi,pobl,cpos,prov,tel1,tel2 
			,slab,fina,gara,area,area_des,servproc,servproc_des,medipeti,medipeti_des 
			,servreal,servreal_des,agen,agen_des,sala,sala_des,cmedreal,medireal,finclusi 
			,prestaci,prestaci_des,priorida,fechacit,fechader,centrder,especder 
			,fsoldem1,fsoldem2,fsoldem3,fsoldem4,fsoldem5,tdemsol1,tdemsol2,tdemsol3,tdemsol4,tdemsol5 
			,fsalida,motisali,indinoga,observac,	finclusihos,	identpres,	tvisita,catalog )
		 SELECT cip,cega,ncita,hicl,cias,ape1,ape2,nomb,fnac,sexo,domi,pobl,cpos,prov,tel1,tel2 
			,slab,fina,gara,area,area_des,servproc,servproc_des,medipeti,medipeti_des 
			,servreal,servreal_des,agen,agen_des,sala,sala_des,cmedreal,medireal,finclusi 
			,prestaci,prestaci_des,priorida,fechacit,fechader,centrder,especder 
			,fsoldem1,fsoldem2,fsoldem3,fsoldem4,fsoldem5,tdemsol1,tdemsol2,tdemsol3,tdemsol4,tdemsol5 
			,fsalida,motisali,indinoga,observac,	finclusihos,	identpres,	tvisita,catalog 
    	FROM LESPCEX WHERE cega = rc.cega AND ncita = rc.ncita;
		--DELETE FROM LESPCEX WHERE cega = rc.cega AND ncita = rc.ncita;
		COUNTER := COUNTER + 1;
		IF COUNTER MOD 1000 = 0 THEN
			COMMIT;
		END IF;
	END LOOP;
	COMMIT;
	COUNTER := 0;
    FOR rc1 IN histo_lesp(dias) LOOP
    	-- inserciones
		DELETE FROM LEHPDEMO WHERE centro = rc1.centro AND norden = rc1.norden;   	
		INSERT INTO LEHPDEMO_HISTO SELECT * FROM LEHPDEMO WHERE centro = rc1.centro AND norden = rc1.norden;
		-- borrados
		DELETE FROM LEHPDEMO WHERE centro = rc1.centro AND norden = rc1.norden;   	
		DELETE FROM SOLAPES WHERE centro = rc1.centro AND claveinicial = rc1.norden;   	
		DELETE FROM CONTINUOS WHERE centro = rc1.centro AND claveinicial = rc1.norden;   	
		-- tratamiento de lesp
    	DELETE FROM LESP_HISTO WHERE centro = rc1.centro AND norden = rc1.norden;
    	INSERT INTO LESP_HISTO 
			(norden,centro,cip,cega,hicl,cias,ape1,ape2,nomb,fnac,sexo,domi,pobl,cpos,prov 
			,tel1,tel2,tel3,slab,fina,gara,serv,secc,acli,cmed,apno_med,finc,cdi1,ddi1 
			,cdi2,ddi2,cpr1,dpr1,cpr2,dpr2,tcir,tane,prio,cinc,preo,frep,fcap 
			,spac,fder,dder,fsde,tdes,fqui,fsal,moti,obse,finchos,motrechazo,fecrechazo,catalog,link) 
		 SELECT norden,centro,cip,cega,hicl,cias,ape1,ape2,nomb,fnac,sexo,domi,pobl,cpos,prov 
			,tel1,tel2,tel3,slab,fina,gara,serv,secc,acli,cmed,apno_med,finc,cdi1,ddi1 
			,cdi2,ddi2,cpr1,dpr1,cpr2,dpr2,tcir,tane,prio,cinc,preo,frep,fcap 
			,spac,fder,dder,fsde,tdes,fqui,fsal,moti,obse,finchos,motrechazo,fecrechazo,catalog,link 
    	 FROM LESP WHERE centro = rc1.centro AND norden = rc1.norden;
		--DELETE FROM LESP WHERE centro = rc1.centro AND norden = rc1.norden;
		IF COUNTER MOD 1000 = 0 THEN
			COMMIT;
		END IF;
	END LOOP;
	COMMIT;

END Inithistorico;
/
