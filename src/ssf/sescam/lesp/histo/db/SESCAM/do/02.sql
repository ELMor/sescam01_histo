CREATE TABLE LEHPDEMO_HISTO
(
  CENTRO      INTEGER                           NOT NULL,
  NORDEN      INTEGER                           NOT NULL,
  FEC_INICIO  DATE                              NOT NULL,
  NDIAS       INTEGER                           NOT NULL,
  TIPO        INTEGER                           NOT NULL,
  ENSITU      INTEGER,
  GRABADIA    DATE,
  GRABAHORA   CHAR(5),
  GRABAUSER   CHAR(20)
 )