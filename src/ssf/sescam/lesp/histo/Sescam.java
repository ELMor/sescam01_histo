package ssf.sescam.lesp.histo;

import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.DriverManager;

import java.sql.SQLException;
import java.util.Properties;
import java.sql.PreparedStatement;

import java.util.HashMap;
import java.util.Iterator;

import java.util.HashSet;

import org.apache.log4j.Logger;

/**
 * <p>Title: Recopilacion Listas de Espera SESCAM</p>
 * <p>Description: Proceso de Carga a Hist�rico</p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */

public class Sescam {
	private Connection toSescam = null;
	Logger log = Logger.getLogger("ssf.sescam.lesp.histo.Sescam");
	private Properties p = null;
	private HashSet hsNorden = new HashSet();
	/**
	 * constructor sin conexi�n.
	 * @param prop
	 * @throws Exception
	 */
	public Sescam(Properties prop) throws Exception {
		p = prop;
		toSescam = 
			DriverManager.getConnection(
				p.getProperty("sescam.url"),
				p.getProperty("sescam.usuario"),
				p.getProperty("sescam.password"));
				
		

	}
	/**
	 * construcci�n con conexi�n
	 * @param prop
	 * @param cnx
	 * @throws Exception
	 */
	public Sescam(Properties prop, Connection cnx) throws Exception {
		p = prop;
		toSescam = cnx;
	}

	
	/**
	 * proceso de inserci�n y borrado en las tablas de LESP
	 * @param ndias
	 * @throws Exception
	 */
	public void run_LESPCEX(int ndias) throws Exception {

		// Obtenci�n de reistros a pasar al hist�rico
		PreparedStatement psLESPCEX = sesPS(gP("sescam.histo.select.lespcex"));
		psLESPCEX.setInt(1, ndias);
		ResultSet rsLESPCEX = psLESPCEX.executeQuery();

		int nItablas =
			new Integer(gP("sescam.histo.lespcex.aux.tablas", "0")).intValue();
		int nDtablas =
			new Integer(gP("sescam.histo.lespcex.tablas", "0")).intValue();
		int count = 0;
		while (rsLESPCEX.next()) {
			String tabla = "";
			int cega = rsLESPCEX.getInt(1);
			int ncita = rsLESPCEX.getInt(2);
			// insercion
			for (int i = 1; i <= nItablas; i++) {
				tabla = gP("sescam.histo.lespcex.aux.tablas." + i + ".nombre");
				runInsert(tabla, cega, ncita);
			}
			runInsert("lespcex_histo", cega, ncita);
			// borrado
			for (int i = 1; i <= nDtablas; i++) {
				tabla = gP("sescam.histo.lespcex.tablas." + i + ".nombre");
				runPurge(tabla, cega, ncita);
			}
			count++;
			if ((count % 1000) == 0) {
				log.info("Procesados " + count + " registros");
				//toSescam.commit();
			}
			runPurge("lespcex", cega, ncita);
		}
		rsLESPCEX.close();
		
//		purgar masterclaves
		String sqlPurgeMasterClave = gP("sescam.histo.delete.lespcex.masterclave");
		PreparedStatement pstPurge = sesPS(sqlPurgeMasterClave);
		pstPurge.execute();
		
		log.info("Insertados " + count + " registros");
	}


	/** 
	 * m�todo para determinar si la entrada en LESP es borrable
	 * debido a que puede tener reinclusiones. Se puede borrar si su 
	 * �ltima reinclusi�n tiene que pasar a hist�rico
	 * @param ndias
	 * @throws Exception
	 */
	public boolean isLoadable(int centro,int norden) throws Exception {
		boolean ret = true;
		PreparedStatement psMaxNorden =  sesPS(gP("sescam.histo.select.maxnorden"));
		// obtener el m�ximo norden de reinclusiones
		psMaxNorden.setInt(1,norden);
		psMaxNorden.setInt(2,centro);
		psMaxNorden.setInt(3,centro);
		ResultSet rsMaxNorden = psMaxNorden.executeQuery();
		String clave = new String(centro + "_" + norden);
		if (rsMaxNorden.next()) {
			int maxNORDEN = rsMaxNorden.getInt(1);
			// si el m�ximo es el mismo se borra
			if (maxNORDEN != norden) {
				ret = hsNorden.contains(clave); 
			}
		}
		hsNorden.add(clave);
				
		return ret;
	}

	/** 
	 * proceso de ejecuci�n en LESP (identico a LESPCEX);
	 * @param ndias
	 * @throws Exception
	 */
	public void run_LESP(int ndias) throws Exception {

		PreparedStatement psLESP = sesPS(gP("sescam.histo.select.lesp"));
		
		psLESP.setInt(1, ndias);
		ResultSet rsLESP = psLESP.executeQuery();

		int nItablas =
			new Integer(gP("sescam.histo.lesp.aux.tablas", "0")).intValue();
		int nDtablas =
			new Integer(gP("sescam.histo.lesp.tablas", "0")).intValue();
		int count = 0;
		while (rsLESP.next()) {
			String tabla = "";
			int cega = rsLESP.getInt(1);
			int norden = rsLESP.getInt(2);
			if (isLoadable(cega,norden)) {
				//				insercion
				for (int i = 1; i <= nItablas; i++) {
					tabla = gP("sescam.histo.lesp.aux.tablas." + i + ".nombre");
					 runInsert(tabla, cega, norden);
				}
				runInsert("lesp_histo", cega, norden);
				// borrado
				for (int i = 1; i <= nDtablas; i++) {
					tabla = gP("sescam.histo.lesp.tablas." + i + ".nombre");
					runPurge(tabla, cega, norden);
				}
				runPurge("lesp", cega, norden);
				count++;
				if ((count % 1000) == 0) {
					log.info("Procesados " + count + " registros");
					//toSescam.commit();
				}
			}
			
		}
		rsLESP.close();
		hsNorden.clear();
		// purgar masterclaves
		String sqlPurgeMasterClave = gP("sescam.histo.delete.lesp.masterclave");
		PreparedStatement pstPurge = sesPS(sqlPurgeMasterClave);
		pstPurge.execute();
		
		
		
		log.info("Insertados " + count + " registros");
	}

	/**
	 * Ejecuci�n de sentencias de inserci�n con borrado previo
	 * @param tabla tabla en la que se va a insertar
	 * @param centro del registro a insertar en la tabla origen
	 * @param clave del registro a insertar en la tabla origen
	 * @return n�mero de registros insertados.
	 * @throws Exception
	 */
	public int runInsert(String tabla, int centro, int clave)
		throws Exception {

		runPurge(tabla, centro, clave);
		PreparedStatement psInsert =
			sesPS(gP("sescam.histo.insert." + tabla));
		psInsert.setInt(1, centro);
		psInsert.setInt(2, clave);
		int count = psInsert.executeUpdate();
		return count;
	}

	/**
	 * Ejecuci�n de sentencias de borrado
	 * @param tabla
	 * @param centro
	 * @param clave
	 * @return
	 * @throws Exception
	 */
	public int runPurge(String tabla, int centro, int clave) throws Exception {
		PreparedStatement psDelete = sesPS(gP("sescam.histo.delete." + tabla));
		psDelete.setInt(1, centro);
		psDelete.setInt(2, clave);
		int count = psDelete.executeUpdate();
		return count;
	}
	/**
	 * Ejecuci�n general
	 * @throws Exception
	 */
		
	public void run() throws Exception {
		try {
			//0. datos de configuracion
			log.info("Obteniendo datos de configuraci�n");
			int ndiasLESP = new Integer(gP("sescam.histo.quirurgica.numdias", "")).intValue();
			int ndiasLESPCEX = new Integer(gP("sescam.histo.consultas.numdias", "")).intValue();
			log.info("Procesando los registros de LESPCEX");
			run_LESPCEX(ndiasLESPCEX);
			log.info("Procesando los registros de LESP");
			run_LESP(ndiasLESP);
			log.info("Proceso de carga a hist�rico terminado");
		} catch (NumberFormatException eNum) {
			log.error("Error en configuracion sescam.histo.numdias");
			throw new Exception("Error en configuracion sescam.histo.numdias");	
		}
		catch (Exception e) {
			log.error("Error general en proceso de carga a hist�rico");
			throw e;
		} finally {
			this.closeStatements();
		}
	}

	/**
	 * wrapper getProperty(string)
	 * @param key
	 * @return
	 */
	private String gP(String key) {
		return p.getProperty(key);
	}

	private String gP(String key, String def) {
		return p.getProperty(key, def);
	}

	 // gestion de PreparedStatements
	/**
	 * contenedor de PreparedStatements clave: sql
	 */
	HashMap hmStatement = new HashMap();
	
	/**
	 * cierre de todos los PreparedStatements que se hayan creado durante el proceso
	 * @throws SQLException
	 */
	private void closeStatements () throws SQLException {
		Iterator it = hmStatement.keySet().iterator();
		while (it.hasNext()) {
			String key = (String) it.next();
			((PreparedStatement) hmStatement.get(key)).close();	 
		}	 
	}
	/**
	 * creaci�n de un nuevo PreparedStatement. Se consulta si est� ya creado
	 * @throws SQLException
	 */
	
	private PreparedStatement sesPS(String sql) throws SQLException {
		PreparedStatement ps = null;
		if (!hmStatement.containsKey(sql)) {
			ps = toSescam.prepareStatement(sql);
			hmStatement.put(sql,ps);
		} else return (PreparedStatement) hmStatement.get(sql); 
		return ps; 
	}

}
