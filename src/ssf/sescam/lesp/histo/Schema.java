/*
 * Created on 16-oct-03
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package ssf.sescam.lesp.histo;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.DatabaseMetaData;
import java.util.Properties;
import java.util.Hashtable;
import ssf.sescam.lesp.histo.db.SQLFileRunner;

import org.apache.log4j.Logger;



/**
 * <p>
 * Title: Proceso de Carga a Histórico del Registro de Listas De Espera de SESCAM
 * </p><p>
 * Description: Comprobación del estado de la Base de Datos de SESCAM
 * </p><p>
 * Copyright: Copyright (c) 2003 Soluziona
 * </p><p>
 * Company: Soluziona
 * </p>
 *
 *
 * @version 1.0 
 */
public class Schema {
	/**
	 * conexión de base de datos a SESCAM
	 */
	public static Connection toSescam = null;
	/**
	 * instancia del Log4j
	 */
	private static Logger log = Logger.getLogger(Schema.class);

	/**
	 * chequear si se puede establecer la conexión del SESCAM
	 * @param p properties de configuración 
	 * @return si la conexión se ha establecido bien o no.
	 */	  
	private static boolean checkConnection(Properties p) {
		try {
		  toSescam = DriverManager.getConnection(
			  p.getProperty("sescam.url"),
			  p.getProperty("sescam.usuario"),
			  p.getProperty("sescam.password"));
		 // log.info("Conexion establecida: Autocommit: " + toSescam.getAutoCommit());
		}
		catch (SQLException e) {
		  log.error("Error obteniendo conexion:", e);
		  toSescam = null;
		  return false;
		}
		return true;
	  }

	  private static boolean checkSchema(Properties p) {
		try {
		  DatabaseMetaData mdSescam = toSescam.getMetaData();
		  boolean ret = true;  
		  int ntablas = new Integer(p.getProperty("sescam.histo.tablas","0")).intValue();
		  for(int i = 1; i <= ntablas && ret; i++) {
			 String tabla = p.getProperty("sescam.histo.tablas."+i+".nombre");
			 ResultSet rs = mdSescam.getTables(null, null, tabla.toUpperCase(), null);
			 ret = (ret && rs.next());
			 rs.close();
		 } 
		  return ret;
		} catch (Exception e) {
		  return false;
		}
	  }

	  private static void createSchema(Properties p) {
		
		try {
		  dropSchema(p);
		  Hashtable ht = new Hashtable();
		  ht.put("{DIAS}", new Integer(p.getProperty("sescam.histo.numdias", "")).toString());
		
		  SQLFileRunner.multipleRun(false, toSescam, p, "sescam.sql.do",
									"Sescam",ht);
		
		}
		catch (Exception e) {
		  e.printStackTrace();
		  try {
			dropSchema(p);
		  }
		  catch (Exception ee) {
		  }
		}
	  }

	  private static void dropSchema(Properties p) {
		try {
		  SQLFileRunner.multipleRun(true, toSescam, p, "sescam.sql.undo",
									"Sescam",null
									);
		}
		catch (Exception e) {
		  log.debug("Drop schema:", e);
		}
	  }
	
	  /**
	   * Chequea la conexion con SESCAM y crea el esquema de datos si es necesario
	   * @param p Archivo de configuracion 'properties'
	   * @param log Logger de salida de mensajes
	   * @return true si esta OK
	   */
	 
	  public static boolean check(Properties p) {
		boolean ret = true;
		if (!checkConnection(p)) {
		  ret = false;
		}
		else {
		  log.info("Conexion con SESCAM comprobada");
		}
		if (!checkSchema(p) && ret) {
		  log.info("No existe esquema, creando tablas");
		  createSchema(p);
		  if (!checkSchema(p)) {
			log.info("Hubo algun error en la creacion del esquema, abortando");
			dropSchema(p);
			ret = false;
		  }
		  else {
			ret = true;
		  } 
		}
   		if (ret == false) {
			try {
			  toSescam.close();
			}
			catch (Exception e) {
			  log.debug("Excepcion", e);
			}
   		}
		return ret;
	  }
	  
}
